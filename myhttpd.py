import http.server
import meccanoid1


#m = meccanoid1.MeccanoidDummy()
m = meccanoid1.Meccanoid()
m.connect()
m.servo(1,0)
m.servo(4,0)




commands = {
    b'eye_lights': lambda *args : m.eye_lights(int(args[0]),int(args[1]),int(args[2])),
    b'servo': lambda *args : m.servo(int(args[0]),int(args[1])),
    b'move': lambda *args : m.move(int(args[0]),int(args[1]))
}


class MyHTTPRequestHandler(http.server.SimpleHTTPRequestHandler):

    def __init__(self, request, client_address, server):
        super().__init__(request, client_address, server)

    def do_POST(self):
        #print("POST")
        #print(len(self.path))
        #print(self.path)
        #print(self.headers)

        lines = []
        for l in self.rfile:

            #strip newline
            l = l[:-1]

            #break on empty line
            if len(l) == 0: break

            lines.append(l)

        command = lines[0]

        #command = self.rfile.readline()[:-1]

        #values = [float(v) for v in command.decode("utf-8").split(',')]
        #left_speed = int((values[1]+0.5*values[0])*-20)
        #right_speed = int((values[1]-0.5*values[0])*-20)
        #m.move(left_speed, right_speed)

        print(command)

        commands[command](*lines[1:])

        #m.eye_lights(7,0,0)

        #if command == b'vooruit':
            #m.move(20,20)
        #if command == b'achteruit':
            #m.move(-20,-20)

    #def end_headers (self):
        #self.send_header('Access-Control-Allow-Origin', '*')
        #super().end_headers()


if __name__ == '__main__':
    http.server.test(HandlerClass=MyHTTPRequestHandler, port=8000, bind="127.0.0.2")
